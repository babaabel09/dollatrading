import Navbar from "./components/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./components/HomePage";
import Faqs from "./components/Faqs";
import WorkMode from "./components/WorkMode";
import About from "./components/About";
import Testimony from "./components/Testimony";
import Footer from "./components/Footer";
import { useEffect, useRef, useState } from "react";
import Contact from "./components/Contact";
import Chat from "./components/Chat";
import { scroller } from "react-scroll";
// import { useWindowScroll } from "react-use";
function App() {
  const [activePathName, setActivePathName] = useState("/");

  const active = (f) => {
    let activePing = document.querySelectorAll(".linksClass");

    activePing.forEach((item) => {
      let animatePing = item.firstElementChild;

      if (animatePing.classList.contains("active")) {
        animatePing.classList.remove("active");
      }
      if (item.innerHTML === f) {
        animatePing.classList.add("active");
      }
    });
  };
  useEffect(() => {
    active();
  }, []);

  const handleClick = (e) => {
    const menu = e.target.parentElement;
    if (menu.classList.contains("linksClass")) {
      setActivePathName(window.location.pathname);

      active(e.target.parentElement.innerHTML);
    }
  };

  function scrollToDiv(offsetNum) {
    const menu = document.querySelector(".menu").childNodes;
    menu.forEach((item) => {
      const pathName = item.firstElementChild.pathname;

      if (window.location.pathname === pathName) {
        const animatePing = item.firstElementChild.childNodes[0];
        if (animatePing.classList.contains("active")) {
          animatePing.classList.remove("active");
        }

        if (pathName === "/") {
          console.log(pathName);
          scroller.scrollTo("homePage", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });
          animatePing.classList.add("active");
        }
        if (pathName === "/about") {
          scroller.scrollTo("about", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });

          animatePing.classList.add("active");
        }
        if (pathName === "/testimonies") {
          // referenceTestimony.current?.scrollIntoView({ behavior: "smooth" });
          scroller.scrollTo("testimony", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });
          animatePing.classList.add("active");
        }
        if (pathName === "/faqs") {
          scroller.scrollTo("faqs", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });
          animatePing.classList.add("active");
        }
        if (pathName === "/contact") {
          scroller.scrollTo("contact", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });
          animatePing.classList.add("active");
          toggleChat();
        }
        if (pathName === "/how-we-work") {
          scroller.scrollTo("workMode", {
            duration: 700,
            delay: 0,
            smooth: true,
            offset: offsetNum, // adjust the offset value as needed
          });
          animatePing.classList.add("active");
        }
      }
    });
  }
  function toggleChat(params) {
    const mailCon = document.querySelector(".mailCon");
    const whatsappCon = document.querySelector(".whatsappCon");

    if (
      mailCon.classList.contains("showChatCons") ||
      whatsappCon.classList.contains("showChatCons")
    ) {
      mailCon.classList.remove("showChatCons");
      whatsappCon.classList.remove("showChatCons");
    } else {
      mailCon.classList.add("showChatCons");
      whatsappCon.classList.add("showChatCons");
    }
  }

  useEffect(() => {
    scrollToDiv(-100);
  }, []);
  useEffect(() => {
    if (window.matchMedia("(min-width: 1280px)").matches) {
      // If media query matches
      scrollToDiv(-100);
    }
  }, [activePathName]);

  return (
    <BrowserRouter>
      <div className="App md:mx-24  mx-5  ">
        <Navbar handleClick={handleClick} toggleChat={toggleChat} />
        <HomePage />
        <WorkMode />

        <About />
        <Testimony />
        <Contact />

        <Faqs />
        <Chat toggleChat={toggleChat} />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
