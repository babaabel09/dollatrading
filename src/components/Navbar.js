import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import logo from "../images/dollaman-d-dark.png";
import { scroller } from "react-scroll";

const Navbar = (props) => {
  const { handleClick, toggleChat } = props;

  // console.log(props);

  function toggleNav() {
    const showNav = document.querySelector(".nav-wrapper");
    const links = document.querySelector(".links");
    const showMenu = document.querySelector(".arrow");
    const arrowRot = document.querySelector(".arrowRot");

    arrowRot.classList.toggle("arrow_rot");

    if (showMenu.classList.contains("showMenu")) {
      showMenu.classList.remove("showMenu");
    } else {
      showMenu.classList.add("showMenu");
    }

    if (showNav.classList.contains("hide-nav")) {
      showNav.classList.remove("hide-nav");
    } else {
      showNav.classList.add("hide-nav");
    }

    if (links.classList.contains("showLinks")) {
      links.classList.remove("showLinks");
    } else {
      links.classList.add("showLinks");
    }
  }

  function scrollerOnClick(x, offsetNum) {
    if (x === "/") {
      scroller.scrollTo("homePage", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
    }
    if (x === "/about") {
      scroller.scrollTo("about", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
    }
    if (x === "/testimonies") {
      // referenceTestimony.current?.scrollIntoView({ behavior: "smooth" });
      scroller.scrollTo("testimony", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
    }
    if (x === "/faqs") {
      scroller.scrollTo("faqs", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
    }
    if (x === "/contact") {
      scroller.scrollTo("contact", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
      toggleChat();
    }
    if (x === "/how-we-work") {
      scroller.scrollTo("workMode", {
        duration: 700,
        delay: 0,
        smooth: true,
        offset: offsetNum, // adjust the offset value as needed
      });
    }
  }

  function removeNavOnMobile() {
    const menu = document.querySelector(".menu").childNodes;
    menu.forEach((item) => {
      item.lastElementChild.addEventListener("click", () => {
        const pathName = item.lastElementChild.pathname;
        toggleNav();
        if (window.matchMedia("(max-width: 1280px)").matches) {
          scrollerOnClick(pathName, -400);
        } else {
          scrollerOnClick(pathName, -100);
        }
      });
    });
  }

  useEffect(() => {
    removeNavOnMobile();
  }, []);

  const handleShowMenu = (e) => {
    toggleNav();
  };

  return (
    <div className="sticky top-0 navbar  z-10">
      <div className="nav-wrapper hide-nav  flex relative  xl:items-center justify-between mt-1.5 py-4  ">
        <div className="logo flex items-center relative">
          <img
            className="logoMage swing object-contain"
            src={logo}
            alt="logo"
          />
          <span className="font-semibold text-2xl tracking-wider logoText">
            olla.manXchange
          </span>
          <span className="best text-md italic tracking-wider">
            We deal with the best!
          </span>
        </div>
        <div className="menu-cont xl:text-left   text-right links xl:mt-3 mt-7      flex justify-between xl:flex-row flex-col ">
          <ul
            className="menu  font-semibold handleActive    flex   xl:items-center items-end xl:flex-row flex-col"
            onClick={handleClick}
          >
            <li className="  relative xl:mr-8 xl:my-0 mb-4 ">
              <Link to="/" className=" linksClass relative" id="active">
                <span class="animate-ping active "></span>
                <span className=" custom-btn btn-5">Home</span>
              </Link>
            </li>
            <li className="relative    xl:mx-8 xl:my-0 mb-4">
              <Link
                // onClick={handleScrollToHowWeWork}
                to="/how-we-work"
                className="linksClass relative "
              >
                <span class="animate-ping "></span>
                <span className=" custom-btn btn-5"> How we work</span>
              </Link>
            </li>
            <li className="relative    xl:mx-8 xl:my-0 mb-4 ">
              <Link
                to="/about"
                className="linksClass relative"
                // onClick={handleScrollToAbout}
              >
                <span class="animate-ping "></span>

                <span className=" custom-btn btn-5">About Us</span>
              </Link>
            </li>
            <li className="relative    xl:mx-8 xl:my-0 mb-4 ">
              <Link
                // onClick={handleScrollToTestimony}
                className="linksClass relative"
                to="/testimonies"
              >
                <span class="animate-ping "></span>

                <span className=" custom-btn btn-5">Testimonials</span>
              </Link>
            </li>
            <li className="relative    xl:mx-8 xl:my-0 mb-4 ">
              <Link
                // onClick={handleScrollToContact}
                to="/contact"
                className="linksClass relative"
              >
                <span class="animate-ping "></span>

                <span className=" custom-btn btn-5">Contact</span>
              </Link>
            </li>

            <li className="relative    xl:ml-8 xl:my-0 mb-4 ">
              <Link
                to="/faqs"
                // onClick={handleScrollToFaqs}
                className="linksClass relative"
              >
                <span class="animate-ping "></span>

                <span className=" custom-btn btn-5">Faqs</span>
              </Link>
            </li>
          </ul>
        </div>
        <div
          className="xl:hidden bg-white  arrow-down arrow animate-bounce p-7 w-6 h-6  flex justify-center items-center  shadow-md text-xl   rounded-full cursor-pointer "
          onClick={handleShowMenu}
        >
          <i className="fa-solid fa-arrow-down  arrowRot"></i>
        </div>
      </div>
      <div className="scroll ">
        <div className="xl:block hidden scroll-text whitespace-nowrap  text-black">
          Hey there! we are always active for trades. We buy all kinds of
          giftcards for all countries, BTC/USDT, and all funds at the best
          market rate. Payment is instant, with no stress and no stories.
        </div>
      </div>
    </div>
  );
};
export default Navbar;
