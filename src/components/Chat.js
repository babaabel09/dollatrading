import { Link } from "react-router-dom";

const Chat = (props) => {
  const toggleChat = props.toggleChat;
  // console.log(x());
  const handleChat = () => {
    toggleChat();
  };

  return (
    <div className="chat flex flex-col">
      <Link to="mailto: babaabel09@gmail.com">
        <i class="mailCon bi bi-envelope-at"></i>
      </Link>
      <Link
        to="https://wa.me/2348147343536?text=hello%20gee"
        className="font-semibold text-white "
      >
        <i class="whatsappCon bi bi-whatsapp my-4"></i>
      </Link>
      <i onClick={handleChat} class=" chatCon bi bi-chat-dots"></i>
    </div>
  );
};

export default Chat;
