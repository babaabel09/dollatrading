import { Link } from "react-router-dom";

const Contact = () => {
  return (
    <div id="contact" className="contact text-white marginsTopAndBotton">
      <div className="w-full text-center contactWrapper">
        <div className="">
          <h2 className=" text-4xl font-semibold">Contact Information</h2>
          <p className="text-md mx-1 mt-5">
            Our highly responsive customer support is available for you 247
          </p>
        </div>
        <div className="flex  mt-5 items-center justify-center  ">
          <div className="">
            <p className="mb-3">
              <Link to="mailto: babaabel09@gmail.com">
                <span className="text-md font-semibold">E-Mail: </span>
                dollamanwealth@gmail.com
              </Link>
            </p>
            <p className="mb-3  text-center ">
              <Link to="tel:+2348147343536">
                <span className="text-md font-semibold">Phone Call: </span>
                2349012487612
              </Link>
            </p>
            <ul className="flex mb-3 justify-center items-center">
              <li className=" instaWrapper">
                <Link>
                  <i class="fa-brands fa-instagram"></i>
                </Link>
              </li>
              <li className="mx-5 fbWrapper">
                <Link>
                  <i class="fa-brands fa-facebook-f"></i>
                </Link>
              </li>
              <li className="twitWrapper">
                <Link>
                  <i class="fa-brands fa-twitter"></i>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Contact;
