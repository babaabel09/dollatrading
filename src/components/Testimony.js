import { useEffect, useRef, useState } from "react";
import btc from "../images/btc.png";
import vid from "../images/flower.mp4";
import img10 from "../images/images (10).jpeg";
import img11 from "../images/images (11).jpeg";
import img12 from "../images/images (12).jpeg";

const Testimony = (props) => {
  const [timer, setTimer] = useState(null);
  const [counter, setCounter] = useState(0);
  const index = useRef(0);

  const swipeData = [
    {
      text: "Thanks to DollaManXchange for saving my business, have lost over 1000usd to fraudsters on the cause of exchanging my iTunes cards to Naira. Now my mind is at rest as I know where to exchange gifts cards to naira without risk. I can't thank you enough.",
      img: img10,
      owner: "Harry Porter",
      title: "Property Owner",
      id: 0,
    },
    {
      text: "What I love about you guys is the fact that you are quick to respond and pay attention to details, I will definitely keep more cards coming. Thanks for the effort you put in",
      img: img11,
      owner: "Damain Marley",
      title: "Property Owner",
      id: 1,
    },
    {
      text: "DollaManXchange have been tested and trusted, I won't be needing another vendor, I'm satisfied trading here.",

      img: img12,
      owner: "Olamide Baddo",
      title: "Property Owner",
      id: 2,
    },
  ];

  function mapswipeData() {
    // let filteredswipe = swipeData.filter((w, ind) => ind === counter);
    let swipeDataMapped = swipeData.map((x, y) => {
      const { text, img, owner, title, id } = x;
      return (
        <div className="main-wrapper w-full" id={id} key={id}>
          <p className="text">{text}</p>
          <div className="images owner-wrapper mt-3 flex">
            <img className="mage " src={img} alt="" />
            <div className="">
              <h4 className="owner mt-2 text-sm font-semibold"> {owner}</h4>
              <p className="title text-sm">{title}</p>
            </div>
          </div>
        </div>
      );
    });
    return swipeDataMapped;
  }
  mapswipeData();
  const handlePrev = () => {
    index.current !== 0
      ? (index.current = index.current - 1)
      : (index.current = 2);
    setCounter(index.current);

    stopInterval();

    autoChangeTestimony();
  };

  const handleNext = () => {
    index.current < 2
      ? (index.current = index.current + 1)
      : (index.current = 0);
    setCounter(index.current);

    stopInterval();

    autoChangeTestimony();
  };

  function autoChangeTestimony() {
    function count() {
      index.current = index.current + 1;

      if (index.current === swipeData.length) {
        index.current = 0;
      }
      setCounter(index.current);
    }
    setTimer(setInterval(count, 10300));
  }
  function stopInterval() {
    clearInterval(timer);
  }

  function activeCir(f) {
    const cir = document.querySelector(".testimony-cir").childNodes;
    const Arr = Array.from(cir);
    Arr.forEach((item) => {
      item.classList.remove("activeCir");

      if (counter === Arr.indexOf(Arr[counter])) {
        cir[counter].classList.add("activeCir");
      } else {
        cir[counter].classList.remove("activeCir");
      }
    });
  }
  useEffect(() => {
    activeCir();
  }, [counter]);

  useEffect(() => {
    autoChangeTestimony();
    document.querySelectorAll(".video").forEach(function (video) {
      video.parentNode.addEventListener("click", function () {
        if (video.paused) {
          video.play();
          this.querySelector(".fa-play").style.display = "none";
          document.getElementById("cover").style.display = "none";
        } else {
          video.pause();
          this.querySelector(".fa-play").style.display = "block";
          document.getElementById("cover").style.display = "block";
        }
      });
      document.getElementById("myVideo").addEventListener("ended", function () {
        document.querySelector(".fa-play").style.display = "block";
        document.getElementById("cover").style.display = "block";
      });
    });
  }, []);

  return (
    <div className="testimony relative marginsTopAndBotton">
      <h2 className=" testimony-header text-center text-4xl font-semibold">
        What our customers say about us
      </h2>
      <div className="lg:grid grid-cols-2 gap-10 flex   lg:flex-row flex-col testimony-content">
        <div className="relative h-full slideContainer ">
          <i class="fa-solid fa-quote-right"></i>
          <div
            style={{ transform: `translate3d(${-counter * 100}%, 0, 0)` }}
            className="testimony-wrapper  relative"
            id="testimony-wrapper"
          >
            {mapswipeData()}
          </div>
          <div className="flex testimony-cir">
            <div className="cir"></div>
            <div className="cir"></div>
            <div className="cir"></div>
          </div>
          <div className="mt-20 mb-5 ml-3 flex nextWrapper">
            <i
              onClick={handlePrev}
              className="next bg-white cursor-pointer fa-solid fa-angles-left text-2xl mr-9  "
            ></i>
            <i
              onClick={handleNext}
              className="next bg-white cursor-pointer fa-solid fa-angles-right text-2xl"
            ></i>
          </div>
        </div>
        <div className="video-wrapper  ">
          {/* <img src={cover} alt="" className="cover" id="cover" /> */}
          <video className="video  " id="myVideo">
            <source src="/media/cc0-videos/flower.webm" type="video/webm" />
            <source src={vid} type="video/mp4" />
            Download the
            <a href="/media/cc0-videos/flower.webm">WEBM</a>
            or
            <a href="/media/cc0-videos/flower.mp4">MP4</a>
            video.
          </video>
          <div className="playpause">
            <i class="fa-solid fa-play"></i>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Testimony;
