import { useEffect, useRef, useState } from "react";

const Faqs = (props) => {
  const [logoDisplay, setlogoDisplay] = useState("");
  const [lastElm, setlastElm] = useState("");
  const [lastWriteUp, setlastWriteUp] = useState("");
  const [lastRot, setlastRot] = useState("");

  let logos = [
    {
      logo: "faqsCards fa-brands fa-apple",
    },
    {
      logo: "faqsCards fa-brands fa-steam",
    },
    {
      logo: "faqsCards fa-brands fa-amazon",
    },
    {
      logo: "faqsCards fa-brands fa-ebay",
    },
  ];

  let about = [
    {
      check: "fa-solid fa-check-double",
      textHeader: "What is the present rate on Crypto and Gift cards? ",
      dropCon: "fa-solid fa-angle-down absolute right-0 cursor-pointer",

      desCon: "fa-solid fa-gift",
      textPara:
        "Rates vary daily and based on the amount of crypto being sold. Kindly refer to our social media page for daily rate updates on gift cards and crypto or click the trade button to speak with our customer service and get the exact rate for your trades.",
    },

    {
      check: "fa-solid fa-check-double",
      textHeader: "How can I trust you? ",
      dropCon: "fa-solid fa-angle-down absolute right-0 cursor-pointer",
      desCon: "fa-solid fa-handshake",
      textPara:
        "DollaMan Xchange is a registered financial service provider. With over 19k+ repeat customers monthly and 4 years of operation, we have been able to establish a reputation as a reliable and trustworthy exchange platform.",
    },
    {
      check: "fa-solid fa-check-double",
      textHeader: "How long does payment take? ",
      dropCon: "fa-solid fa-angle-down  absolute right-0 cursor-pointer",
      desCon: "fa-solid fa-receipt",
      textPara: " Payments are processed instantly.",
    },
    {
      check: "fa-solid fa-check-double",
      textHeader: "What should I send when I want to sell a gift card? ",
      dropCon: "fa-solid fa-angle-down absolute right-0 cursor-pointer",
      desCon: "fa-solid fa-thumbs-up",
      textPara:
        "Depending on the card we might need any of the following; Receipts, Country or first 4 Digits of the card.",
    },
    {
      check: "fa-solid fa-check-double",
      textHeader: "How will I get my payment? ",
      dropCon: "fa-solid fa-angle-down absolute right-0 cursor-pointer",
      desCon: "fa-solid fa-piggy-bank",
      textPara:
        " Payments will be processed into the customer’s desired account upon completion and confirmation of trade.",
    },
  ];
  const handleHeight = (e) => {
    if (e.target.classList.contains("faqs-textHeader")) {
      writeUp(e);
    }
  };

  function writeUp(f) {
    let addHeight = f.target.parentElement.parentElement;
    let showWriteUp = f.target.parentElement.nextElementSibling;
    let rotate = f.target.childNodes[2];

    setlastElm(addHeight);
    setlastWriteUp(showWriteUp);
    setlastRot(rotate);
    // if (f.target.classList.contains("about-textHeader")) {
    if (addHeight.classList.contains("addFaqsHeight")) {
      addHeight.classList.remove("addFaqsHeight");
    } else {
      addHeight.classList.add("addFaqsHeight");
    }

    // Handle singles
    if (lastElm !== addHeight) {
      if (addHeight.classList.contains("addFaqsHeight")) {
        lastElm.classList.remove("addFaqsHeight");
      }
      if (!showWriteUp.classList.contains("showWrite-up-wrapper")) {
        lastWriteUp.classList.remove("showWrite-up-wrapper");
      }
      if (!rotate.classList.contains("rot-fa-angle-down")) {
        lastRot.classList.remove("rot-fa-angle-down");
      }
    }
    // End
    if (rotate.classList.contains("rot-fa-angle-down")) {
      rotate.classList.remove("rot-fa-angle-down");
    } else {
      rotate.classList.add("rot-fa-angle-down");
    }

    if (!showWriteUp.classList.contains("showWrite-up-wrapper")) {
      showWriteUp.classList.add("showWrite-up-wrapper");
    } else {
      showWriteUp.classList.remove("showWrite-up-wrapper");
    }
    // }
  }
  function odd() {
    const oddAbout = about.filter((x, ind) => ind % 2 !== 1);
    return oddAbout;
  }
  function even() {
    const evenAbout = about.filter((x, ind) => ind % 2 === 1);
    return evenAbout;
  }

  let faqsOdd = odd().map((x, ind) => {
    const { check, textHeader, dropCon, desCon, textPara } = x;

    return (
      <div onClick={handleHeight} className="relative  faqsTextWrapper ">
        <div
          className="relative  faqs-text "
          // ref={ref}
          id={ind}
        >
          <div className="faqs-textHeader  ">
            <i className={`ms-5 ${check}`}></i>
            <h3 className="header truncate ">{textHeader}</h3>
            <i className={dropCon}></i>
          </div>
        </div>
        <div className="write-up-wrapper flex">
          <span className=" ">
            <i className={desCon}></i>
          </span>
          <p className="write-up leading-relaxed tracking-wide  text-neutral-500">
            {textPara}
          </p>
        </div>
      </div>
    );
  });
  let faqsEven = even().map((x, ind) => {
    const { check, textHeader, dropCon, desCon, textPara } = x;
    return (
      <div onClick={handleHeight} className="relative  faqsTextWrapper ">
        <div
          className="relative  faqs-text  "
          // ref={ref}
          id={ind}
        >
          <div className="faqs-textHeader  ">
            <i className={`ms-5 ${check}`}></i>
            <h3 className="header truncate ">{textHeader}</h3>
            <i className={dropCon}></i>
          </div>
        </div>
        <div className="write-up-wrapper flex">
          <span className=" ">
            <i className={desCon}></i>
          </span>
          <p className="write-up leading-relaxed   text-neutral-500">
            {textPara}
          </p>
        </div>
      </div>
    );
  });

  let combineFaqs = about.map((x, ind) => {
    const { check, textHeader, dropCon, desCon, textPara } = x;
    return (
      <div onClick={handleHeight} className="relative  faqsTextWrapper ">
        <div
          className="relative  faqs-text  "
          // ref={ref}
          id={ind}
        >
          <div className="faqs-textHeader  ">
            <i className={`ms-5 ${check}`}></i>
            <h3 className="header truncate ">{textHeader}</h3>
            <i className={dropCon}></i>
          </div>
        </div>
        <div className="write-up-wrapper flex">
          <span className=" ">
            <i className={desCon}></i>
          </span>
          <p className="write-up leading-relaxed   text-neutral-500">
            {textPara}
          </p>
        </div>
      </div>
    );
  });

  useEffect(() => {
    const { faqsWriteUp1, faqsWriteUp2, dropDown } = onPageLoad();

    if (document.querySelector(".faqsTextWrapper") !== null) {
      setlastElm(faqsWriteUp1);
      setlastWriteUp(faqsWriteUp2);
      setlastRot(dropDown);
    }

    onPageLoad();
    changeLogo();
  }, []);

  // functions about write up that stays at initial load of page
  function onPageLoad() {
    let faqsWriteUp1 =
      document.querySelector(".faqsTextWrapper").childNodes[0].parentElement;
    // console.log(faqsWriteUp1);
    let faqsWriteUp2 = document.querySelector(".faqsTextWrapper").childNodes[1];

    let dropDown =
      document.querySelector(".faqsTextWrapper").childNodes[0].childNodes[0]
        .childNodes[2];

    faqsWriteUp1.classList.add("addFaqsHeight");
    faqsWriteUp2.classList.add("showWrite-up-wrapper");
    dropDown.classList.add("rot-fa-angle-down");
    return { faqsWriteUp1, faqsWriteUp2, dropDown };
  }

  // Fuction to change logos

  function changeLogo() {
    let i = 3;
    function count() {
      let singleCard = logos.filter((x, y) => y === i);

      setlogoDisplay(
        singleCard.map((x, ind) => {
          const { logo } = x;
          return <i className={logo}></i>;
        })
      );

      i--;

      if (i === -1) {
        i = 3;
      }
    }
    let timerId = setInterval(count, 3000);
    // setTimeout(() => {
    //   clearInterval(timerId);
    // }, 4000);
    // logoDisplay = count();
    count();
  }

  return (
    <div id="faqs" className="faqs relative  marginsTopAndBotton">
      <div className="flex flex-row items-center justify-center">
        <h2 className=" text-4xl font-semibold truncate mr-3">
          Frequently Asked Questions
        </h2>

        <div className="faqsCardsWrapper ml-auto   h-full text-end">
          <h3 className="">{logoDisplay}</h3>
        </div>
      </div>
      <div className="faqsWrapper flex lg:flex-row flex-col  gap-x-8">
        <div className="faqsWriteCont lg:hidden block ">
          <div className=" faqs-textWrapper">{combineFaqs}</div>
        </div>

        <div className="faqsWriteCont  lg:block hidden">
          <div className=" faqs-textWrapper">{faqsOdd}</div>
        </div>
        <div className="faqsWriteCont lg:block hidden ">
          <div className=" faqs-textWrapper">{faqsEven}</div>
        </div>
      </div>
    </div>
  );
};

export default Faqs;
