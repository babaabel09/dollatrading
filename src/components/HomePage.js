import { Link } from "react-router-dom";
import QR from "../images/QR.png";

import bitCoin from "../images/btc1.png";
import GiftCards from "./GiftCards";

const HomePage = (props) => {
  let resizeTimer;
  window.addEventListener("resize", () => {
    document.body.classList.add("resize-animation-stopper");
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => {
      document.body.classList.remove("resize-animation-stopper");
    }, 400);
  });

  return (
    <div id="homePage" className=" homePage  relative  z-0 ">
      <div className=" lg:grid  h-full grid-cols-2 gap-6 flex flex-col  ">
        <div className="textWrapper mt-7 w-3/4 lg:w-full flex flex-col justify-start items-start">
          <h3 className="leading-10 font-bold  text-3xl">
            Welcome to DollaMan Wealth
          </h3>
          <p className=" xl:text-md  leading-normal mt-5 mb-3 text-neutral-500">
            Dolla.manXchange is your one-stop platform for trading your
            cryptocurrencies and giftcard exchange for instant cash today.
          </p>
          <div className="relative">
            <span class="animate-ping active absolute left-0"></span>
          </div>
          <div className="whatsAppBtn flex  w-full">
            <button className="custom-btn btn-12 tradeBtn">
              <Link
                to="https://wa.me/2349012487588?text=Hello%20Dolla.man%2C%20I%20want%20more%20info%20for%20trading%20my%20gift%20cards%20and%20crypto.%20Let's%20trade%20here%20I%20was%20directed%20from%20your%20webpage."
                className="font-semibold text-white "
              >
                <span>Click to Trade</span>
                <span className="whitespace-nowrap"> Trade on Whatsapp</span>
              </Link>
            </button>

            <button className="custom-btn btn-12 tradeBtn tradeBtn2 ml-10 sm:ml-14">
              <Link
                to="https://wa.link/434i4e"
                className="font-semibold text-white "
              >
                <span>Click to Trade</span>
                <span className="whitespace-nowrap "> Whatsapp Line 2</span>
              </Link>
            </button>
          </div>
        </div>
        {/* <div className="bitcardWrapper  ">
          <img className="bitCards   swing" src={cards} alt="bitCoin cards" />
        </div> */}
        {<GiftCards />}
      </div>
      <div className="QRcode mt-3  absolute  text-center top-0 right-0">
        <div className="relative">
          <span class="animate-ping active absolute right-0"></span>
        </div>
        <p className="text-sm">Scan to Trade</p>
        <img className="h-20 w-20" src={QR} alt="QR code" />
      </div>
      <div>
        <div className="btc">
          <i class="btc1 h-9 w-9 object-contain rounded-full fa-solid fa-bitcoin-sign"></i>
        </div>
        <div className="btc">
          <i class="btc2 h-9 w-9 object-contain rounded-full fa-solid fa-bitcoin-sign"></i>
        </div>
        <div className="btc">
          <i class="btc3 h-9 w-9 object-contain rounded-full fa-solid fa-bitcoin-sign"></i>
        </div>
      </div>
    </div>
  );
};
export default HomePage;
